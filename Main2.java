import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

class MyForm extends JFrame {
    private JTextField textField;
    private JButton button;

    public MyForm() throws HeadlessException {
        this.setSize(400, 500);
        this.setLayout(null);
        this.textField = new JTextField();
        this.button = new JButton("Show reverse from file");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String fileName = textField.getText();
                try (FileReader fileReader = new FileReader(fileName);
                     BufferedReader bufferedReader = new BufferedReader(fileReader)
                ){
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);
                        stringBuilder.append(System.lineSeparator());
                    }
                    System.out.println(stringBuilder.reverse().toString());
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });
        this.textField.setBounds(10, 10, 200, 40);
        this.button.setBounds(10, 100, 200, 40);
        this.add(textField);
        this.add(button);
    }

}

public class Main2 {
    public static void main(String[] args) {
        MyForm myForm = new MyForm();
        myForm.setVisible(true);
    }
}
