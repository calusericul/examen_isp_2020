class A {
}

class B extends A {
    private long t;

    public void b() {
        C c = new C();
    }
}

class C {
}

class D {
    private B b;
    private E e;

    public void met1(int i) {
    }
}

class E {
    public void met2() {
    }
}

class F {
    private D d;
}

public class Main1 {
    public static void main(String[] args) {

    }
}
